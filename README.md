#!/bin/bash

# Kommandos zum direkten Starten eines lokalen kixotrs auf z.B. Ubuntu, Erklärung s. unten

``` 
su root
mkdir -p /var/uniba.de/docker
cd /var/uniba.de/docker
curl https://api.rz.uni-bamberg.de/api/docker/v1/current/public/kixotrs-leer.tar.gz > kixotrs-leer.tar.gz
tar -xzf kixotrs-leer.tar.gz 

container_dir="/var/uniba.de/docker"
container="kixotrs-leer"
image="kixotrs-leer"
pfad="$container_dir"/"$container"/"$image"
cd $pfad

apt-get install docker.io

#docker build --no-cache -t $image:1 .
docker build -t $image:1 .

volumes="$container_dir"/"$container"/volumes
#docker rm -f "$container"
sudo docker run -d \
--name="$container" \
-p  127.0.0.1:223:22 \
-p  127.0.0.1:4433:443 \
-v $volumes/opt:/opt \
-v $volumes/var/www:/var/www \
-v $volumes/var/lib/mysql:/var/lib/mysql \
-v $volumes/root/uniba.de/share:/root/uniba.de/share \
$image:1

https://127.0.0.1:4433/otrs/index.pl
root@localhost
aendern
``` 

# Vorraussetzungen
* OS au f dem Docker läuft z.B. ubuntu 14.04
* ssh-pubkey für Login auf docker-container

``` 
su root
#als root (unklar ob das notwendig ist, bisher nur mit "root" getestet)

```

# Schritt 1 ggf. Docker installieren

``` 
apt-get install docker.io
#(auf Ubuntu 14.04)
``` 

# Schritt 2: Docker-Container erstellen

```
container_dir="/var/uniba.de/docker"
#Speicherort für Nutzdaten, ggf. beliebig umbenennen

container="kixotrs-leer"
#Name des Containers container="kixotrs-leer" beliebig umbenennen

image="kixotrs-leer"

mkdir -p $container_dir
pfad="$container_dir"/"$container"/"$image"
cd $pfad

#in Datei start.sh ssh pubkey hinterlegen, wird später für das login in den container benötigt

docker build --no-cache -t $image:1 .
#image erstellen, nur einmal initial notwendig, wenn es nicht vorhanden ist oder bei Änderungen im Dockerfile 

volumes="$container_dir"/"$container"/volumes
sudo docker run -d \
--name="$container" \
-p  127.0.0.1:223:22 \
-p  127.0.0.1:4433:443 \
-v $volumes/opt:/opt \
-v $volumes/var/www:/var/www \
-v $volumes/var/lib/mysql:/var/lib/mysql \
-v $volumes/root/uniba.de/share:/root/uniba.de/share \
$image:1

#container basierend auf image starten
#ports mappen: von aussen 4431 ist im container 4431
##-p  127.0.0.1:4431:443 
##die IP Adresse muss eine des lokalen Systems sein z.B. geht auch 127.0.0.1

#die Verzeichnisse mit den Daten legen wir auf den Host, nicht in den container,
#dann sind sie unabhängig und einfach sicherbar
##z.B. -v $volumes/opt:/opt 
```


# Einlogen im Container

jetzt können wir uns einlogggen im container

ssh -p 223 root@127.0.0.1

https://127.0.0.1:4433/otrs/index.pl

Benutzer:       root@localhost

Passwort:       aendern








